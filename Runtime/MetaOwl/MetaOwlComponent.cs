using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.IO;


namespace MetaOwl
{
    public class MetaOwlComponent : MonoBehaviour
    {

        private float _delayBetweenObservableChecks = 0.5f;
        private float _currentObservableTick = 0;
        private float _currentFPSTick = 0;
        private Camera _camera;
        private int _currentFPSRate = 0;
        private int _frameCount = 0;
        
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void OnEnable()
        {
            Application.logMessageReceived += ProcessLogs;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            Application.logMessageReceived -= ProcessLogs;
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene _, LoadSceneMode __)
        {
            _camera = Camera.main;
        }

        private void ProcessLogs(string logString, string stackTrace, LogType type)
        {
            return;
            MetaOwlSdk.Log(logString, stackTrace, (LoggingSeverity) type);
        }

        private void Start()
        {
            if (MetaOwlSdk.IsFirstTimeRun)
            {
                Debug.Log("Registering a new device");
                MetaOwlSdk.SendDiscoveryEvent();
                PlayerPrefs.SetInt("metaOwlSdk.Initialized", 1);
            }

            _currentObservableTick = Time.time;
            _currentFPSTick = Time.time;
            _currentFPSRate = Application.targetFrameRate;

        }

        private void Update()
        {
            if (Time.time - _currentObservableTick > _delayBetweenObservableChecks && MetaOwlSdk.PlayerIsAlive)
            {
                MetaOwlSdk.SendObservables(_camera);
                _currentObservableTick = Time.time;
            }
            _frameCount++;
            var currentFPSTick = Time.time - _currentFPSTick;
            if (currentFPSTick > 1)
            {
                _currentFPSTick = Time.time;
                _currentFPSRate = _frameCount;
                _frameCount = 0;
            }
        }

        private void OnApplicationFocus( bool hasFocus )
        {
            OnApplicationPause(!hasFocus);
        }

        private void OnApplicationPause( bool pauseStatus )
        {
            if (MetaOwlSdk.GameIsPaused == pauseStatus) return;
            MetaOwlSdk.SendEvent(pauseStatus?Action.PauseGame:Action.UnpauseGame);
            MetaOwlSdk.GameIsPaused = pauseStatus;
        }
        

        internal void Send(Record record)
        {
            if (!MetaOwlSdk.RunInEditor && Application.isEditor) return;
            
            record.ID = MetaOwlSdk.AppInstallId;
            record.Timestamp = DateTime.Now;
            record.GameVersion = Application.version;
            record.Token = MetaOwlSdk.Config.AccountToken;
            
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.Converters.Add(new JsonStringEnumConverter());
            var json = JsonConvert.SerializeObject(record, jsonSerializerSettings);
            var recordRaw = Encoding.UTF8.GetBytes(json);
            StartCoroutine(SendRaw(recordRaw));
        }
        
        private IEnumerator SendRaw(byte[] recordRaw)
        {
            var request = new UnityWebRequest(MetaOwlSdk.URL, "POST");
            request.SetRequestHeader("Content-Type", "application/json");

            request.uploadHandler = new UploadHandlerRaw(recordRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
            
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                MetaOwlSdk.DisableLogReporting = true;
                Debug.LogError($"MetaOwl SDK: {request.error}");
                MetaOwlSdk.DisableLogReporting = false;
            }
            request.Dispose();
        }
    }
}