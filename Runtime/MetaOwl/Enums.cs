namespace MetaOwl
{
    public enum Model
    {
        GameSession,
        Player,
        Enemy,
        Item,
        Resource,
        User,
        MainQuest,
        SideQuest,
        ExitSpot
    }
    
    public enum Action
    {
        StartGame,
        CloseGame,
        PauseGame,
        UnpauseGame,
        CreateAccount,
        Login,
        Logout,
        TakeCollectable,
        StartLevel,
        AbortLevel,
        FinishLevel,
        ReachLevelCheckpoint,
        StartTurn,
        EndTurn,
        
        EnterZone,
        ExitZone,

        Destroy,

        LevelUp,
        UpdateXp,
        
        StartQuest,
        EndQuest,
        SabotageQuest,
        UseAbility,
        Heal,
        
        SetPosition,
        Win,
        Lose,
        Attack,
        UseItem,
        GetDamage,
        GetHeal,
        Spawn,
        Dead,
        Observe,
        Profiling
    }

    public enum LoggingSeverity
    {
        /// <summary>
        ///   <para>LogType used for Errors.</para>
        /// </summary>
        Error,
        /// <summary>
        ///   <para>LogType used for Asserts. (These could also indicate an error inside Unity itself.)</para>
        /// </summary>
        Assert,
        /// <summary>
        ///   <para>LogType used for Warnings.</para>
        /// </summary>
        Warning,
        /// <summary>
        ///   <para>LogType used for regular log messages.</para>
        /// </summary>
        Log,
        /// <summary>
        ///   <para>LogType used for Exceptions.</para>
        /// </summary>
        Exception,
    }
}