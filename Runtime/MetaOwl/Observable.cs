using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MetaOwl
{

    internal struct Observable
    {
        public string Uid;
        public Collider[] Colliders;
        public MeshRenderer[] MeshRenderers;
        public SkinnedMeshRenderer[] SkinnedMeshRenderers;
        
    }

    public class Observables
    {
        private readonly List<Observable> _observables = new ();
        
        public void Subscribe(GameObject gameObject, string uid)
        {
            var colliders = gameObject.GetComponentsInChildren<Collider>().ToArray();
            if (colliders.Length == 0)
            {
                Debug.LogWarning($"{gameObject.name} and it's children does not have any collider, object will be omitted!");
                return;
            }
            
            var meshRenderers = gameObject.GetComponentsInChildren<MeshRenderer>().ToArray();
            var skinnedMeshRenderers = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>().ToArray();
            if (meshRenderers.Length + skinnedMeshRenderers.Length == 0)
            {
                Debug.LogWarning($"{gameObject.name} and it's children does not have any MeshRenderer/SkinnedMeshRenderer, object will be omitted");
                return;
            }
            _observables.Add(new Observable
            {
                Uid = uid,
                Colliders = colliders,
                MeshRenderers = meshRenderers,
                SkinnedMeshRenderers = skinnedMeshRenderers,
            });
        }
        
        public void UnSubscribe(string uid)
        {
            foreach (var observable in _observables)
            {
                if (string.Equals(observable.Uid, uid))
                {
                    _observables.Remove(observable);
                    return;
                }
            }
        }
        
        public IEnumerable<string> GetVisibleObjects(Camera camera)
        {
            foreach (var observable in _observables)
            {
                if(IsObjectVisible(observable, camera))
                    yield return observable.Uid;
            }


        }

        private bool IsObjectVisible(Observable observable, Camera camera)
        {
            var isSeen = false;
            foreach (var collider in observable.Colliders)
            {
                isSeen = collider != null && IsSeenByCamera(collider, camera);
                if(isSeen)
                    break;
            }

            return isSeen && IsMeshVisible(observable);

        }

        private bool IsMeshVisible(Observable observable)
        {
            return observable.MeshRenderers.Any(mesh => mesh.enabled) || observable.SkinnedMeshRenderers.Any(mesh => mesh.enabled);
        }
        private bool IsSeenByCamera(Collider collider, Camera cam)
        {
            var planes = GeometryUtility.CalculateFrustumPlanes(cam);

            return GeometryUtility.TestPlanesAABB(planes , collider.bounds);
            
            
        }
    }
}