using UnityEngine;

namespace MetaOwl
{
    public class MetaOwlConfig : ScriptableObject
    {
        public string AccountToken;
    }
}
