using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MetaOwl
{
    public static class EnumExtension
    {
        public static string ToNameValue(this Enum enumValue)
        {
            var input = enumValue.ToString();
            return System.Text.RegularExpressions.Regex
                .Replace(input, "([A-Z])", " $1", System.Text.RegularExpressions.RegexOptions.Compiled)
                .Trim();
        }
    }

    internal class JsonStringEnumConverter : StringEnumConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value.GetType().IsEnum)
            {
                writer.WriteValue((value as Enum).ToNameValue());
                return;
            }

            base.WriteJson(writer, value, serializer);
        }
    }
}