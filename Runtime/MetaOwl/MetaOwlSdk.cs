using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;


namespace MetaOwl
{
    internal class Record
    {
        public const string Name = "Record";
        public string ID;
        public string Token;
        public DateTime Timestamp;
        public string GameVersion;
    }

    internal class DiscoveryEvent : Record
    {
        public new const string Name = "Discovery Event";

        [CanBeNull] public string DeviceModel;
        public DeviceType DeviceType;
        [CanBeNull] public string ProcessorType;
        public int SystemMemorySize;
        [CanBeNull] public string OperatingSystem;
    }

    internal class ProfilingEvent : Record
    {
        public new const string Name = "Profiling Event";
        public long MemoryUsed;
        public int FrameRate;
    }

    internal class Event : Record
    {
        public new const string Name = "Event";
        public Action Action;
        [CanBeNull] public Dictionary<string, string> ExtraParams;
    }
    
    internal class LogMessage : Record
    {
        public new const string Name = "Log Message";
        public string Message;
        public string StackTrace;
        public LoggingSeverity Severity;
        public string Stacktrace;
    }

    public static class MetaOwlSdk
    {
        internal static MetaOwlConfig Config;

        internal const string URL = "https://ingest.meta-owl.io/send_data";
        internal static bool GameIsPaused = false;
        public static bool RunInEditor = false;
        public static bool IsFirstTimeRun => !PlayerPrefs.HasKey("metaOwlSdk.Initialized");

        private static float _lastPerformanceTick = 0;
        public static float PerformanceRate = 2f;
        
        public static Observables Observables;
        
        
        [CanBeNull] internal static string AppInstallId;
        [CanBeNull] private static string _playerId;
        [CanBeNull] private static Transform _playerTransform;
        internal static bool PlayerIsAlive;
        
        private static ushort _currentLevelNumber = 1;
        
        public static bool DisableLogReporting = false;

        private static MetaOwlComponent _metaOwlComponent;

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            Config = Resources.Load<MetaOwlConfig>("MetaOwlConfig");
            Debug.Log($"MetaOwl Initialized with token: {Config.AccountToken}");

            Observables = new Observables();
            InstantiateMetaOwlGameObject();

            AppInstallId = SetInstallId();

            if (IsFirstTimeRun)
            {
                Debug.Log("Registering a new device");
                SendDiscoveryEvent();
                PlayerPrefs.SetInt("metaOwlSdk.Initialized", 1);
            }
            SendEvent(Action.StartGame);
        }

        private static string SetInstallId()
        {
            var appInstallId = SystemInfo.deviceUniqueIdentifier;
            if (appInstallId != SystemInfo.unsupportedIdentifier) return appInstallId;
            
            if (!PlayerPrefs.HasKey("metaOwlSdk.installId"))
            {
                var uuid = Guid.NewGuid().ToString();
                PlayerPrefs.SetString("metaOwlSdk.installId", uuid);
            }
            appInstallId = PlayerPrefs.GetString("metaOwlSdk.installId");

            return appInstallId;      
        }

        public static void SetPlayerId(string playerId)
        {
            _playerId = playerId;
        }

        private static void InstantiateMetaOwlGameObject()
        {
            var metaOwlGameObject = new GameObject("Meta Owl");
            _metaOwlComponent = metaOwlGameObject.AddComponent<MetaOwlComponent>();
        }

        public static void SendDiscoveryEvent()
        {
            var discoveryEvent = new DiscoveryEvent
            {
                DeviceModel = SystemInfo.deviceModel,
                DeviceType = SystemInfo.deviceType,
                ProcessorType = SystemInfo.processorType,
                SystemMemorySize = SystemInfo.systemMemorySize,
                OperatingSystem = SystemInfo.operatingSystem,
            };
            _metaOwlComponent.Send(discoveryEvent);
        }

        public static void SendObservables(Camera cam)
        {
            if(_playerTransform == null) return;
            var observables =  string.Join(",", Observables.GetVisibleObjects(cam).ToArray() ); 
            var extraParams = new Dictionary<string, string>
            {
                {"id", _playerId},
                {"observables", observables}
                
            };
            extraParams.Add("player_position", _playerTransform.position.ToString());
            SendEvent(Action.Observe, extraParams);
        }

        // Profiling Event
        public static void SendProfilingEvent(int frameRate)
        {
            var performanceInterval = Time.time - _lastPerformanceTick;
            if (performanceInterval > PerformanceRate)
                return;
            
            var profilingEvent = new ProfilingEvent
            {
                MemoryUsed = GC.GetTotalMemory(true),
                FrameRate = frameRate
            };
            
            _metaOwlComponent.Send(profilingEvent);
            
            _lastPerformanceTick = Time.time;
        }

        public static void StartMatch(string matchId)
        {
            StartLevel(0, matchId);
        }
        
        public static void EndMatch()
        {
            EndLevel(0);
        }
        
        public static void StartLevel(ushort levelNumber, [CanBeNull] string matchId = null)
        {
            _currentLevelNumber = levelNumber;
            SendEvent(Action.StartLevel, new Dictionary<string, string>
            {
                {"levelNumber", levelNumber.ToString()},
                {"id", matchId}
            });
        }
        
        public static void EndLevel(ushort levelNumber)
        {
            _currentLevelNumber = levelNumber;
            SendEvent(Action.StartLevel, new Dictionary<string, string>
            {
                {"levelNumber", levelNumber.ToString()}
            });
        }
        
    
        public static void PlayerSpawn(string playerId, Transform playerTransform, string playerType = null)
        {
            _playerTransform = playerTransform;
            playerTransform.gameObject.AddComponent<ZoneDetector>();
            PlayerSpawn(playerId, playerTransform.position, playerType);
        }
        public static void PlayerSpawn(string playerId, Vector3 playerPosition, string playerType = null)
        {
            _playerId = playerId;
            PlayerIsAlive = true;
            
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.Player.ToNameValue()},
                {"id", _playerId},
                {"player_position", playerPosition.ToString()},
            };
            
            if (!string.IsNullOrEmpty(playerType))
            {
                extraParams.Add("player_type", playerType);
            }
            SendEvent(Action.Spawn, extraParams);
        }
        
        
        public static void UseAbility(string name)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"name", name},
                {"id", _playerId}
            };
            SendEvent(Action.UseAbility, extraParams);
        }
        
        public static void Heal()
        {
            var extraParams = new Dictionary<string, string>
            {
                {"id", _playerId}
            };
            SendEvent(Action.Heal, extraParams);
        }

        public static void SpawnMainQuest(string questId, Vector3 position)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.MainQuest.ToNameValue()},
                {"id", questId},
                {"quest_position", position.ToString()},
            };
            SendEvent(Action.Spawn, extraParams);
        }
        
        public static void SpawnSideQuest(string questId, Vector3 position)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.SideQuest.ToNameValue()},
                {"id", questId},
                {"quest_position", position.ToString()},
            };
            SendEvent(Action.Spawn, extraParams);
        }
        
        public static void StartMainQuest(string questId)
        {
            StartMainQuest(questId, _playerId);
        }

        public static void StartMainQuest(string questId, string playerId)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.MainQuest.ToNameValue()},
                {"id", questId},
                {"player_id", playerId}
            };
            SendEvent(Action.StartQuest, extraParams);
        }
        
        public static void FinishMainQuest(string questId)
        {
            StartMainQuest(questId, _playerId);
        }
        public static void FinishMainQuest(string questId, string playerId)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.MainQuest.ToNameValue()},
                {"id", questId},
                {"player_id", playerId}
            };
            SendEvent(Action.EndQuest, extraParams);
        }
        
        public static void SabotageMainQuest(string questId)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.MainQuest.ToNameValue()},
                {"id", questId},
            };
            SendEvent(Action.SabotageQuest, extraParams);
        }

        public static void StartSideQuest(string questId)
        {
            StartSideQuest(questId, _playerId);
        }
        public static void StartSideQuest(string questId, string playerId)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.SideQuest.ToNameValue()},
                {"id", questId},
                {"player_id", playerId}
            };
            SendEvent(Action.StartQuest, extraParams);
        }
        
        public static void FinishSideQuest(string questId)
        {
            FinishSideQuest(questId, _playerId);
        }
        
        public static void FinishSideQuest(string questId, string playerId)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.SideQuest.ToNameValue()},
                {"id", questId},
                {"player_id", playerId}

            };
            SendEvent(Action.EndQuest, extraParams);
        }
        
        public static void SabotageSideQuest(string questId)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.SideQuest.ToNameValue()},
                {"id", questId},
            };
            SendEvent(Action.SabotageQuest, extraParams);
        }
        
        
        public static void SpawnExitSpot(string id, Vector3 position)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.ExitSpot.ToNameValue()},
                {"id", id},
                {"exit_spot_position", position.ToString()},
            };
            SendEvent(Action.Spawn, extraParams);
        }
        
        public static void StartNextLevel()
        {
            StartLevel((ushort) (_currentLevelNumber+1));
        }
        
        public static void FinishCurrentLevel(int? score = null)
        {
            FinishLevel(_currentLevelNumber, score);
        }
        public static void FinishLevel(int levelNumber, int? score = null)
        {
            SendEvent(Action.FinishLevel, new Dictionary<string, string>
            {
                {"levelNumber", levelNumber.ToString()},
                {"score", score.ToString()}
            });
        }
        
        public static void AbortCurrentLevel(int? score = null, Vector3? playerPosition = null)
        {
            AbortLevel(_currentLevelNumber, score);
        }
        public static void AbortLevel(int levelNumber, int? score = null, Vector3? playerPosition = null)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"levelNumber", levelNumber.ToString()},
                {"score", score.ToString()}
            };
            if (playerPosition.HasValue)
            {
                extraParams.Add("player_position", playerPosition.Value.ToString());
            }
            SendEvent(Action.AbortLevel, extraParams);
        }

        
        
        public static void PlayerGetDamage()
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.Player.ToNameValue()},
                {"id", _playerId},
            };
            SendEvent(Action.GetDamage, extraParams);
        }
        
        
        public static void PlayerGetHeal()
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.Player.ToNameValue()},
                {"id", _playerId},
            };
            SendEvent(Action.GetHeal, extraParams);
        }

        public static void PlayerSetPosition(Vector3 playerPosition)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.Player.ToNameValue()},
                {"id", _playerId},
                {"player_position", playerPosition.ToString()},
            };
            SendEvent(Action.SetPosition, extraParams);
        }
        
        public static void PlayerDead(Vector3? playerPosition = null)
        {
            PlayerIsAlive = false;

            var extraParams = new Dictionary<string, string>
            {
                {"type", Model.Player.ToNameValue()},
                {"id", _playerId},
            };
            if (playerPosition.HasValue)
            {
                extraParams.Add("player_position", playerPosition.Value.ToString());
            }
            SendEvent(Action.Dead, extraParams);
        }
        
        public static void PlayerEnterZone(string name)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"zone_name", name},
                {"id", _playerId},
            };
            SendEvent(Action.EnterZone, extraParams);
        }
        
        public static void PlayerExitZone(string name)
        {
            var extraParams = new Dictionary<string, string>
            {
                {"zone_name", name},
                {"id", _playerId},
            };
            SendEvent(Action.ExitZone, extraParams);
        }
        
        
        
        public static void EnemyDead(string enemyCategory = null)
        {
            SendEvent(Action.Dead, new Dictionary<string, string>()
            {
                {"type", Model.Enemy.ToNameValue()},
                {"category", enemyCategory},
            });
        }
        
        public static void NamedEnemyDead(string enemyId = null)
        {
            SendEvent(Action.Dead, new Dictionary<string, string>()
            {
                {"type", Model.Enemy.ToNameValue()},
                {"id", enemyId},
            });
        }

        public static void ReachedLevelCheckpoint()
        {
            SendEvent(Action.ReachLevelCheckpoint);
        }

        public static void TakeCollectable()
        {
            SendEvent(Action.TakeCollectable);
        }
        
        public static void SendEvent(Action action, [CanBeNull] Dictionary<string, string> extraParams = null)
        {
            var eventEntry = new Event
            {
                Action = action,
                ExtraParams = extraParams
            };
            _metaOwlComponent.Send(eventEntry);
        }

        public static void Log(string message, string stackTrace, LoggingSeverity severity)
        {
            if(DisableLogReporting) return;
            var logEntry = new LogMessage
            {
                Message = message,
                StackTrace = stackTrace,
                Severity = severity
            };
            _metaOwlComponent.Send(logEntry);
        }
    }
}
