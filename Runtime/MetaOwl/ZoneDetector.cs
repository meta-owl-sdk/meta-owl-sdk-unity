using UnityEngine;

namespace MetaOwl
{
    public class ZoneDetector: MonoBehaviour
    {
        private void OnTriggerEnter(Collider collision)
        {
            if (collision.gameObject.CompareTag("MapZone"))
            {
               MetaOwlSdk.PlayerEnterZone(collision.gameObject.name);
            }
        }
        
        private void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.CompareTag("MapZone"))
            {
                MetaOwlSdk.PlayerExitZone(collision.gameObject.name);
            }
        }
    }
}