using UnityEngine;
using UnityEditor;

namespace MetaOwl
{
    public class ManageToken : EditorWindow
    {
        public string tokenString;
        private string _saveButton = "Save";

        public void RefreshTokenValue()
        {
            var config = GetMetaOwlConfig();
            if(config is null)
                return;
            tokenString = config.AccountToken;
        }

        void OnGUI()
        {
            tokenString = EditorGUILayout.TextField("Game Token", tokenString);
            if (GUILayout.Button(_saveButton))
            {
                var config = GetMetaOwlConfig();
                if (config is null)
                {
                    Debug.LogError("Couldn't find config file");
                    return;
                }
                config.AccountToken = tokenString;

                EditorUtility.SetDirty(config);
                AssetDatabase.Refresh();

                Close();
            }
        }

        private MetaOwlConfig GetMetaOwlConfig()
        {
            var config = AssetDatabase.LoadAssetAtPath(MetaOwlEditor.ConfigFilePath, typeof(MetaOwlConfig)) as MetaOwlConfig;
            if (config is null)
            {
                Debug.LogError("Couldn't find config file");
            }

            return config;
        }

    }
}