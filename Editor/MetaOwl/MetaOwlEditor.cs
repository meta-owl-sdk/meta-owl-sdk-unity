using System.IO;
using UnityEditor;
using UnityEngine;

namespace MetaOwl
{
    public static class MetaOwlEditor
    {
        public const string ConfigFilePath = @"Assets/Resources/MetaOwlConfig.asset";

        static MetaOwlEditor() {
            MetaOwlSdk.RunInEditor = EditorPrefs.GetBool("metaOwlSdk.RunInEditor", MetaOwlSdk.RunInEditor);
            EditorApplication.delayCall += () => {
                Menu.SetChecked("MetaOwl/Run In Editor", MetaOwlSdk.RunInEditor);
            };
        }
        
        
        [InitializeOnLoadMethod]
        private static void InitializeOnLoad()
        {
            if (!Directory.Exists(@"Assets/Resources"))
            {
                Directory.CreateDirectory(@"Assets/Resources");
            }

            var config = ScriptableObject.CreateInstance(typeof(MetaOwlConfig)) as MetaOwlConfig;
            AssetDatabase.CreateAsset(config, ConfigFilePath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
 
        [MenuItem("MetaOwl/Run In Editor")]
        private static void ToggleAction()
        {
            Debug.Log($"Toggling Run in editor variable from {MetaOwlSdk.RunInEditor} ");
            MetaOwlSdk.RunInEditor = !MetaOwlSdk.RunInEditor;
            Menu.SetChecked("MetaOwl/Run In Editor", MetaOwlSdk.RunInEditor);
            EditorPrefs.SetBool("metaOwlSdk.RunInEditor", MetaOwlSdk.RunInEditor);
        }

        [MenuItem("MetaOwl/Clear Initialization")]
        private static void ClearInitialization()
        {
            PlayerPrefs.DeleteKey("metaOwlSdk.Initialized");
        }
        
        [MenuItem("MetaOwl/Manage Token")]
        private static void OpenManageTokenWindow()
        {
            var window = EditorWindow.GetWindow(typeof(ManageToken)) as ManageToken;
            window.RefreshTokenValue();
            window.Show();
        }
    }
}